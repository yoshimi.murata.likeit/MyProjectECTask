<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>購入確認</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="IndexServlet">Project:Object</a>

  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="ItemListServlet">商品一覧</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="CartServlet">カート</a>
      </li>
      <c:choose>
      <c:when test="${userInfo.login_id!='admin'}">
      <li class="nav-item">
        <a class="nav-link" href="UserBuyDetailServlet?id=${UserData.id}">購入履歴</a>
      </li>
      </c:when>
      <c:otherwise>
      <li class="nav-item">
        <a class="nav-link" href="UserListServlet">ユーザー情報</a>
      </li>
      </c:otherwise>
      </c:choose>
      <li class="nav-item">
        <a class="nav-link" href="LogoutServlet">ログアウト</a>
      </li>
      <c:if test="${userInfo.login_id=='admin' }">
      <li class="nav-item">
      	<a class="nav-link" href="UserManagementServlet">管理者ページ</a>
      </li>
      </c:if>
    </ul>
    <span class="navbar-text">
      LoginUser:${userInfo.name}
    </span>
  </div>
</nav>
<!--/ ue-->
</body>
</html>