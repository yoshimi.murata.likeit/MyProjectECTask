var plus = function(){

	// おおもとの要素を指定
	var group = document.getElementById('hoge');

	// 子要素に含まれるtr要素の数を取得z
	var count = group.getElementsByTagName('tr').length;

	// 最初のtr要素を基準に親ノードを取得
	var parent_node = group.getElementsByTagName('tr')[0].parentNode;

	// 新しいtr要素を生成
	var tr_node = document.createElement('tr');

	// 生成したtr要素に属性値を追加
	tr_node.setAttribute('id', 'tr' + (count + 1));

	// 生成したtr要素をノードに追加
	parent_node.appendChild(tr_node);

	// 入力枠を追加する
	document.getElementById('tr' + (count + 1)).innerHTML = '<th>ItemByTrackList:' + (count + 1) + '</th><td id="td' + (count + 1) + '"><input type="text" name="ByFileTrackList" size="60"></td>';

}