<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<jsp:include page="header.jsp" />
<h3>購入が完了しました</h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>購入日時</th>
                    <th>合計金額</th>
                 </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${resultBDB.create_date}</td>
                    <td>${resultBDB.total_price}円</td>
                </tr>
            </tbody>
        </table>
    </div>
   <div class="row">
   <c:forEach var="buyIDB" items="${buyIDBList}">
        <div class="col-2">
            <div class="card" >
              <img src="img/${buyIDB.file_name}" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a>${buyIDB.name}</a>
                    <br>
                    ${buyIDB.price}円
                    <br>
                    ${buyIDB.detail_text}
                </p>
              </div>
            </div>
        </div>
    </c:forEach>
    </div>
</body>
</html>