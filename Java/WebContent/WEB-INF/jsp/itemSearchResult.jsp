<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<div class="container">
    <div class="row center">
        <h4>検索結果一覧：X件</h4>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                </p>
              </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>
           <div class="col-3">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                </p>
              </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card" >
                <img src="img/one.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">
                        <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                        <br>
                        1234円
                        <br>
                        スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
