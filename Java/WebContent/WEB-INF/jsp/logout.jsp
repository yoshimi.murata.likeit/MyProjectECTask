<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="IndexServlet">Project:Object</a>

  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="cart.html">カート</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="UserData.html">ユーザー情報</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.html">ログイン・アウト</a>
        <!--ログイン時はログアウト。ログインしていない場合ログインが表示されるように-->
      <li>
          <input type="search" autocomplete="on" list="list">
          <input type="submit" value="検索">
      </li>
    </ul>
    <span class="navbar-text">
      (ログインしてるユーザー情報)
    </span>
  </div>
</nav>
<!--/ ue-->

<!--main -->
    <h4>ログアウトしました</h4>
<!--/main-->
</body>
</html>
