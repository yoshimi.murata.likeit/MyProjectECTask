<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<H4>ユーザー情報</H4>
                <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>登録日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
               <c:forEach var="user" items="${userList}" >
                   <tr>
                     <c:choose>
                     <c:when test="${userInfo.login_id=='admin' }">
                     <td>${user.login_id}</td>
                     <td>${user.name}</td>
                     <td>${user.create_date}</td>
                     <td>
                       <a class="btn btn-warning" href="UserBuyDetailServlet?id=${user.id}">購入履歴</a>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdataServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
                     </c:when>
                     <c:otherwise>
						<c:if test="${userInfo.login_id==user.login_id}">
                     <td>${user.login_id}</td>
                     <td>${user.name}</td>
                     <td>${user.create_date}</td>
                     <td>
		       		   <a class="btn btn-warning" href="UserBuyDetailServlet?id=${user.id}">購入履歴</a>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdataServlet?id=${user.id}">更新</a>
                     </td>
						</c:if>
                     </c:otherwise>
                     </c:choose>
                   </tr>
                   </c:forEach>
               </tbody>
             </table>
           </div>
<!--/-->
<!-- -->
<br>
<br>
<c:choose>
<c:when test="${userInfo.login_id=='admin' }">
<%--(admin時のみ表示) --%>
    <H4>ユーザー検索</H4>
<form action="UserListServlet" method="post">
<table class="table">
  <tbody>
    <tr>
      <th scope="row">LoginId</th>
      <td colspan="2"><input type="text" name="seachLoginId"></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">UserName</th>
      <td colspan="2"><input type="text" name="seachUserName"></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">CreateDate:Start~End</th>
      <td colspan="2"><input type="date" name="seachCreateDate"></td>
      <td>～</td>
      <td><input type="date" name="seachCreateDateEnd"> </td>
    </tr>
  </tbody>
</table>
    <button type="submit" class="btn btn-lg btn-primary">検索</button>
</form>
</c:when>
</c:choose>
</body>
</html>
