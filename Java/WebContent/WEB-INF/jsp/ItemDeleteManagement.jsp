<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--main-->
<H4>商品マスタ:アイテム削除</H4>
<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <img src="img/${itemDetail.file_name}" width="180" height="180">
        </div >
    </div>
    <h4>商品ID:${itemDetail.id}
    <br>商品名:${itemDetail.name}
    <br>を本当に削除しますか？</h4>
    <form class="form-signin" action="ItemDeleteManagementServlet" method="post">
		<input type="hidden" name="id" value="${itemDetail.id}">
        <button type="submit" class="btn btn-danger">削除</button>
    </form>
</div>
<!--/main-->
    </body>
</html>