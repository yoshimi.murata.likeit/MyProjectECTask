<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->


<br>
<br>
<form>
<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <img src="img/hotrats50.jpg" width="140" height="210">
            <input type="file" width="140" heigth="210">
        </div >

        <div class="col-sm-8">
            <p><input type="text" value="album title"></p>
            <input type="number" value="3245">
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <textarea cols="50" rows="10">スロバキアの尖鋭ギタリストDavid Kollarが2019年にリリースした最新作。
            ソ連の伝説的映画監督のAndrei Tarkovskyの詩にインスパイアされ、多くの明示的および暗示的なレベルで表現されたアコースティック色の強いサウンド。
            フランスのトランペット奏者Erik Truffaz、ノルウェーのトランペット奏者Arve Henriksonとのコラボレーションは 奥深い。
            <br>※当店では、決済終了（入金確認後）後、即日発送致します｡
            <br>※発送料は当店負担（無料）とさせて頂きます。</textarea>
        </div>
    </div>
    <br>
    <div class="row">
        <br>
        <div class="col-sm-6">
            <h5>視聴</h5>
            <div class="video">
                <iframe src="https://www.youtube.com/embed/rODqxzNkbMo"
                frameborder="0" allowfullscreen></iframe>
            </div>
            <input type="text" value="youtubeURL">
        </div>

        <div class="col-sm-6">
            <table border="1">
                <tr>
                    <td><input type="text" value="song1"></td>
                </tr>
                <tr>
                    <td><input type="text" value="song2"></td>
                </tr>
                <tr>
                    <td><input type="text" value="song3"></td>
                </tr>
                <tr>
                    <td><input type="text" value="song4"></td>
                </tr>
            </table>
        </div>
    </div>
    </div>
    <a href="UserManagement.html" class="btn btn-success">更新</a>
    </form>
</body>
</html>
