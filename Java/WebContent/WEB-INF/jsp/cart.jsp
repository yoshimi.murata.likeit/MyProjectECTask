<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>買い物カート</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<jsp:include page="header.jsp" />
<!--main-->
<div class="container">
    <div class="row center">
        <h4>買い物カート</h4>
        ${cartActionMessage}
    </div>
	<form action="ItemDeleteServlet" method="post">
    <div class="row">
        <c:forEach var="item" items="${Cart}" varStatus="status">
        <div class="col-2">
            <div class="card" >
              <img src="img/${item.file_name}" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a >${item.name}</a>
                    <br>
                    ${item.price}円
                    <br>
                    ${item.detail_text}
                    <br>
                    <input type="checkbox" value="${item.id}" name="DeleteItemIdList"> <label >削除</label>
                    <input type="hidden" value="${item.id}" name="BuyItemId">
                </p>
              </div>
            </div>
        </div>
        </c:forEach>
    </div>

	<br>
	<button type="submit" class="btn btn-primary">チェックした商品を削除する</button>
    </form>
    	<form action="BuyServlet" method="post">
        <c:forEach var="item" items="${Cart}" varStatus="status">
        	<input type="hidden" value="${item.id}" name="BuyItemID">
        	<input type="hidden" value="${item.price}" name="BuyItemPrice">
        </c:forEach>
        	<input type="hidden" value="${UserData.id}" name="BuyUserID">
        	<button type="submit" class="btn btn-secondary">レジに進む</button>
        </form>
</div>
</body>
</html>
