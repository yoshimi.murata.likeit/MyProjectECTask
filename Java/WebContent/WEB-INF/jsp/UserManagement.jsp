<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
<div class="container">
    <H4>管理者用商品マスター</H4>
    <a class="btn btn-danger" href ="ItemCreateManagementServlet">商品情報追加</a>
    <br>
    <br>
<h3>登録商品一覧</h3>
    <div class="row">
	<c:forEach var="item" items="${itemList}" >
        <div class="col-3">
            <div class="card" >
              <img src="img/${item.file_name}" class="card-img-top" alt="image">
              <div class="card-body">
                <p class="card-text">
                    <a>${item.name}</a>
                    <br>
                    ${item.artist_name}
                    <br>
                    ${item.price}円
                    <br>
                    ${item.detail_text}
                </p>
                	<a href="ItemDetailManagementServlet?id=${item.id}" class="btn btn-primary">詳細</a>
                    <a href="ItemUpdateManagementServlet?id=${item.id}" class="btn btn-success">更新</a>
                    <a href="ItemDeleteManagementServlet?id=${item.id}" class="btn btn-danger">削除</a>
              </div>
            </div>
        </div>
	</c:forEach>
    </div>
</div>
</body>
</html>
