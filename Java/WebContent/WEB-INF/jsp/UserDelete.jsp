<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
<!--main-->

        <div class="text center">
        <h1>ユーザ削除確認</h1>
        </div>

        <h4>ログインID:${user.login_id},ユーザーID:${user.id}を本当に削除してよろしいでしょうか。</h4>

		<!-- フォーム -->
		<div>
			<form class="form-signin" action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">
          		<button class="btn btn-primary btn-sml" type="submit">削除</button>
          		<!-- submitボタンを押すとUserDeleteServletのPOSTメソッドの内容が実行される -->
            </form>
        </div>

		<div>
			<button type="button" class="btn btn-primary btn-sml" onclick="history.back()">キャンセル</button>
        </div>

		<!-- /フォーム -->
<!--/main-->

    </body>
</html>
