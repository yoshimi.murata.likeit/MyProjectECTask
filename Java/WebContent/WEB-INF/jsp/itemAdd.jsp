<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--main-->
<div class="container">
    <div class="row center">
        <h4>カート一覧</h4>
    </div>
    <div class="row">
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card" >
              <img src="img/one.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a href="buy.html">Project:object 1978 Poughkeepsie</a>
                    <br>
                    1234円
                    <br>
                    スロバキア発、尖鋭ギタリストDavid Kollarの2019年最新作
                    <br>
                    <input type="checkbox">
                </p>
              </div>
            </div>
        </div>
    </div>


    <a class="btn btn-lg" href="#">削除</a>
    <a href="Cart.html" class="btn btn-primary">購入手続きに進む</a>


</div>
<!--/main-->
</body>
</html>