<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>購入最終確認</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--main-->
<div class="container">
    <div class="row center">
        <h4>購入</h4>
    </div>

    <div class="row">
        <c:forEach var="item" items="${Cart}" varStatus="status">
        <div class="col-2">
            <div class="card" >
              <img src="img/${item.file_name}" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a >${item.name}</a>
                    <br>
                    ${item.detail_text}
                </p>
              </div>
            </div>
        </div>
        </c:forEach>
     </div>
<br>
<table class="table">
  <thead>
    <tr>
      <th scope="col">商品名</th>
      <th scope="col">単価</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="item" items="${Cart}" varStatus="status">
    <tr>
      <td>${item.name}</td>
      <td>${item.price}円</td>
    </tr>
    </c:forEach>
    <tr>
    	<td></td>
    	<td>合計:${bdb.total_price}円</td>
    </tr>
  </tbody>
</table>
	<form action="BuyResultServlet" method="post">
		<input type="hidden" value="${UserData.id}" name="BuyUserId">
		<input type="hidden" value="${bdb.total_price}" name="TotalPrice">
		<c:forEach var="item" items="${Cart}" varStatus="status">
		<input type="hidden" value="${item.price}" name="BuyItemPrice">
		<input type="hidden" value="${item.id}" name="BuyItemId">
		</c:forEach>
		<button type="submit" class="btn btn btn-primary">購入を確定する</button>
	</form>
</div>
</body>
</html>