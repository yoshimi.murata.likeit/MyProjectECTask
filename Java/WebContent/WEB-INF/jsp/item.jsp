<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>商品ページ</title>
	<%-- item.jsp --%>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />

<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <img src="img/${itemDetail.file_name}" width="180" height="180">
        </div >

        <div class="col-sm-8">
            <p>${itemDetail.name}</p>
            <a>${itemDetail.price}円</a>
            <form action="ItemAddServlet" method="POST">
            	<input type="hidden" value="${ItemId}" name="ItemId">
            	<button type="submit" class="btn btn-primary">カートへ入れる</button>
            </form>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <a>${itemDetail.detail_text}</a>
            <br>※当店では、決済終了（入金確認後）後、即日発送致します｡
            <br>※発送料は当店負担（無料）とさせて頂きます。
        </div>
    </div>
    <br>
    <div class="row">
        <br>
        <div class="col-sm-6">
            <h5>視聴</h5>
            <div class="video">
                <iframe src="${itemDetail.youtube_url}"
                frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

        <div class="col-sm-6">
            <table border="1">
            <c:forEach var="itl" items="${ITL}" >
            	<tr>
                    <td>${itl.track_id}:${itl.track_name}</td>
                </tr>
            </c:forEach>
            </table>
        </div>
    </div>
</div>

</body>
</html>