<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="/baselayout/header.jsp" />

<div class="container">
    <div class="center">
        <h5>イチオシ商品</h5>
        <p>収録時間 | 41:27 Released February 9,1970<a href="ItemServlet?id=7">購入ページ</a> </p>
        <div class="video">
            <iframe src="https://www.youtube.com/embed/niabuEIKYyc"
            frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <br>
    <div class="row center">
        <h3 class=" col s12 light">商品一例</h3>
    </div>


    <div class="row">
    <c:forEach var="item" items="${itemList}" >
        <div class="col-3">
            <div class="card" >
              <img src="img/${item.file_name}" class="card-img-top" alt="image">
              <div class="card-body">
                <p class="card-text">
                    <a href="ItemServlet?id=${item.id}">${item.name}</a>
                    <br>
                    ${item.artist_name}
                    <br>
                    ${item.price}円
                    <br>
                    ${item.detail_text}
                </p>
              </div>
            </div>
        </div>
		</c:forEach>

    </div>

    </div>
</body>
</html>