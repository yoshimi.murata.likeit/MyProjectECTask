<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
<jsp:include page="header.jsp" />
<form action="ItemCreateManagementServlet" method="post" enctype="multipart/form-data" >
<H4>商品マスタ:アイテム追加</H4>
	<table class="table">
  		<thead>
    		<tr>
      			<th scope="col">#</th>
      			<th scope="col">DATA</th>
    		</tr>
  		</thead>
  		<tbody>
    		<tr>
      			<th scope="row">ItemByName</th>
      			<td colspan="2"><input type="text" name="CreateItemByName" autofocus></td>
    		</tr>
    		<tr>
    			<th scope="row">ItemByArtistName</th>
    			<td colspan="2"><input type="text" name="ItemByArtistName" autofocus></td>
    		</tr>
    		<tr>
      			<th scope="row">ItemByPrice</th>
      			<td colspan="2"><input type="number" name="CreateItemByPrice" autofocus></td>
    		</tr>
    		<tr>
    			<th scope="row">URL</th>
    			<td colspan="2"><input type="text" name="CreateItemByURL" autofocus></td>
    		</tr>
    		<tr>
    			<th scope="row">ItemByDetailText</th>
    			<td colspan="2"><textarea cols="50" rows="10" name="CreateItemByText"></textarea></td>
    		</tr>
    		<tr>
    			<th scope="row">ItemByFile</th>
<!--   			<td colspan="2"><input type="text" name="CreateItemByFileName" autofocus></td> -->
				<td colspan="2"><input type="file" name="CreateItemByFileName" autofocus>
    		</tr>
    		<tr>
			<table id="hoge">
				<tr id="tr1">
				<th scope="row">ItemByTrackList</th>
					<td id="td1" colspan="2"><input type="text" name="ByFileTrackList" size="60"></td>
				</tr>
			</table>
				<a href="javascript: void(0);" onclick="plus();">＋入力枠を追加する</a>
			<tr>

  		</tbody>
	</table>
	<br>
	<br>
	<button type="submit" class="btn btn-danger">追加</button>
</form>

</body>
</html>
