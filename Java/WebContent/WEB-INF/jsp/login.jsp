<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<c:if test="${errMsg != null}" >
		<div class="alert alert-success" role="alert">
			${errMsg}
		</div>
	</c:if>

	<form action="LoginServlet" method="post">
<table class="table">
  <tbody>
    <tr>
      <th scope="row">LoginId</th>
      <td colspan="2"><input type="text" name="loginId" required autofocus></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Password</th>
      <td colspan="2"><input type="password" name="password" required autofocus></td>
      <td></td>
    </tr>
  </tbody>
</table>
    <button type="submit" class="btn btn-lg btn-primary">ログイン</button>
    </form>
    <a class="btn btn-lg btn-secondary" href="UserCreateServlet" >新規登録</a>
</body>
</html>