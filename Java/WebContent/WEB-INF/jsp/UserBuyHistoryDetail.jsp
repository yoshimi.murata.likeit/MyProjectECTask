<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
<!-- main-->
<H4>購入詳細</H4>
            <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>購入日時</th>
                   <th>購入金額</th>
                 </tr>
               </thead>
               <tbody>
                   <tr>
                     <td>${BDBBB.formatDate}</td>
                     <td>総額${BDBBB.total_price}円</td>
                   </tr>
               </tbody>
             </table>
           </div>
   <div class="row">
   <c:forEach var="idblb" items="${IDBLB}">
        <div class="col-2">
            <div class="card" >
              <img src="img/${idblb.file_name}" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">
                    <a>${idblb.name}</a>
                    <br>
                    ${idblb.price}円
                    <br>
                    ${idblb.detail_text}
                </p>
              </div>
            </div>
        </div>
    </c:forEach>
    </div>
</body>
</html>