<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
<!-- main-->
<H4>購入履歴</H4>
            <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>購入日時</th>
                   <th>購入金額</th>
                   <th>詳細</th>
                 </tr>
               </thead>
               <tbody>
               <c:forEach var="uiibl" items="${UserIdInfoBuyList}">
                   <tr>
                     <td>${uiibl.formatDate}</td>
                     <td>${uiibl.total_price}円</td>
                     <td><a href="UserBuyHistoryDetailServlet?buyid=${uiibl.id}" class="btn btn-info rounded-circle p-0" style="width:2rem;height:2rem;">≫</a></td>
                   </tr>
               </c:forEach>
               </tbody>
             </table>
           </div>

</body>
</html>
