<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%--
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
--%>
<!-- main-->
	<c:if test="${errMsg != null}" >
		<div class="alert alert-success" role="alert">
			${errMsg}
		</div>
	</c:if>
	<form action="UserCreateServlet" method="post">
    	<h3>ユーザー新規登録</h3>

	<table class="table">
  		<tbody>
    		<tr>
      			<th scope="row">ログインID</th>
      			<td colspan="2"><input type="text" name="createLoginId"></td>
    		</tr>
    		<tr>
      			<th scope="row">パスワード</th>
      				<td colspan="2"><input type="text" name="createLoginPass"></td>
    		</tr>
    		<tr>
      			<th scope="row">パスワード(確認)</th>
      			<td colspan="2"><input type="text" name="confirmLoginPass"></td>
    		</tr>
    		<tr>
      			<th scope="row">ユーザー名</th>
      			<td colspan="2"><input type="text" name="createUserName"></td>
    		</tr>
  		</tbody>
	</table>
    <button type="submit" class="btn btn-lg btn-primary">登録</button>
    </form>
</body>
</html>
