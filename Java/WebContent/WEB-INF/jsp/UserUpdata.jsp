<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>

<form action="UserUpdataServlet" method="post">
    <input type="hidden" name="id" value="${user.id}">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">DATA</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">LoginId</th>
      <td colspan="2">${user.login_id}</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">UserName</th>
      <td colspan="2"><input value="${user.name}" type="text" name="UpdateName" autofocus></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Password</th>
      <td colspan="2"><input type="text" name="UpdatePass"></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Password:Re</th>
      <td colspan="2"><input type="text" name="UpdatePass2"></td>
      <td></td>
    </tr>
  </tbody>
</table>
<button type="submit" class="btn btn-primary">更新する</button>
</form>

</body>
</html>