<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<h3>商品全一覧（入荷順）</h3>
<form action="ItemSearchServlet" method="post">
<table class="table">
  <tbody>
    <tr>
      <th scope="row">商品名</th>
      <td colspan="2"><input type="text" name="SearchItemName"></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">バンド名</th>
      <td colspan="2"><input type="text" name="SearchItemArtistName"></td>
      <td></td>
    </tr>
  </tbody>
</table>
  <button type="submit" class="btn btn-lg btn-primary">商品検索</button>
  </form>
<br>
<div class="container">
    <div class="row">
<c:forEach var="item" items="${itemList}" >
        <div class="col-3">
            <div class="card" >
              <img src="img/${item.file_name}" class="card-img-top" alt="image">
              <div class="card-body">
                <p class="card-text">
                    <a href="ItemServlet?id=${item.id}">${item.name}</a>
                    <br>
                    ${item.artist_name}
                    <br>
                    ${item.price}円
                    <br>
                    ${item.detail_text}
                </p>
              </div>
            </div>
        </div>
</c:forEach>
    </div>
</div>
</body>
</html>
