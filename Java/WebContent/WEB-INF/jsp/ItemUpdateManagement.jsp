<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>商品ページ</title>
	<%-- item.jsp --%>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--/ ue-->
<br>
<br>
<form action="ItemUpdateManagementServlet" method="post" enctype="multipart/form-data">
<div class="container">
	<h4>商品マスタ:商品情報更新</h4>
    <div class="row">
        <div class="col-sm-2">
            <img src="img/${ItemDetail.file_name}" width="180" height="180">
<!--        <input type="text" name="UpdateItemByFileName" placeholder="/jpg"> -->
            <input type="file" name="UpdateItemByFileName" autofocus>
        </div >
        <div class="col-sm-8">
            <p><input type="text" name="UpdateItemByName" autofocus value="${ItemDetail.name}"></p>
	        <p><input type="text" name="UpdateItemByArtistName" autofocus value="${ItemDetail.artist_name}"></p>
            <a><input type="number" name="UpdateItemByPrice" autofocus value="${ItemDetail.price}">円</a>
            <input type="hidden" value="${ItemDetail.id}" name="UpdateItemById">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <textarea cols="50" rows="5" name="UpdateItemByDetailText">${ItemDetail.detail_text}</textarea>
            <br>※当店では、決済終了（入金確認後）後、即日発送致します｡
            <br>※発送料は当店負担（無料）とさせて頂きます。
        </div>
    </div>
    <br>
    <div class="row">
        <br>
        <div class="col-sm-6">
            <h5>視聴</h5>
            <div class="video">
                <iframe src="${ItemDetail.youtube_url}"
                frameborder="0" allowfullscreen></iframe>
            </div>
            <input type="text" name="UpdateItemByURL" autofocus placeholder="/url">
        </div>
        <div class="col-sm-6">
            <table border="1">
            <c:forEach var="ITL" items="${ItemTrackList}" >
            	<tr>
                    <td><input type="text" name="UpdateTrackList" value="${ITL.track_name}" ><pre>${ITL.track_name}</pre></td>
                </tr>
            </c:forEach>
            </table>
        </div>
    </div>
    <button type="submit" class="btn btn-success">更新</button>
</div>
</form>
</body>
</html>