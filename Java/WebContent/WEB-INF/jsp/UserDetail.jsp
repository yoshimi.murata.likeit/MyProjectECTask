<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
	<title>TOP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--ue-->
<jsp:include page="header.jsp" />
<!--main-->
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">DATA</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">LoginId</th>
      <td colspan="2">${user.login_id}</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">Name</th>
      <td colspan="2">${user.name}</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">create_date</th>
      <td colspan="2">${user.create_date}</td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">update_date</th>
      <td colspan="2">${user.update_date}</td>
      <td></td>
    </tr>
  </tbody>
</table>

    </body>
</html>