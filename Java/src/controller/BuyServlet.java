package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Buy;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String BuyUserId=request.getParameter("BuyUserID");
		String[] BuyItemPrice = request.getParameterValues("BuyItemPrice");
		String[] BuyItemID = request.getParameterValues("BuyItemID");

		HttpSession session = request.getSession();
		ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
		int totalPrice = Item.getTotalItemPrice(Cart);

		Buy bdb = new Buy();
		bdb.setTotal_price(totalPrice);
		session.setAttribute("bdb", bdb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);
	}

}
