package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDAO;
import model.Item;

/**
 * Servlet implementation class ItemAddServlet
 */
@WebServlet("/ItemAddServlet")
public class ItemAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			String ItemId=request.getParameter("ItemId");
			System.out.println("商品ID:"+ItemId+"をカートに追加します");
			ItemDAO itemdao=new ItemDAO();
			Item ITEM=itemdao.findByItem(ItemId);
			//カートを取得
			ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
			//ArrayList<Item> Cart =new ArrayList<Item>();

			//セッションにカートがない場合カートを作成
			if (Cart == null) {
				Cart = new ArrayList<Item>();
			}
			Cart.add(ITEM);
			session.setAttribute("Cart", Cart);
			request.setAttribute("cartActionMessage", "商品を追加しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
