package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDAO;
import dao.BuyDetailDAO;
import model.Buy;
import model.BuyDetail;
import model.Item;
import model.User;

/**
 * Servlet implementation class BuyResultServlet
 */
@WebServlet("/BuyResultServlet")
public class BuyResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			String BuyUserId=request.getParameter("BuyUserId");
			String TotalPrice=request.getParameter("TotalPrice");
			String[] BuyItemPrice = request.getParameterValues("BuyItemPrice");
			String[] BuyItemId = request.getParameterValues("BuyItemId");
			int BuyItemID;

			int buyId =BuyDAO.BuyItem(BuyUserId,TotalPrice);
			//buyテーブルに、ユーザーID、総額を追加：購入処理

			BuyDetail bddb = new BuyDetail();
			  for (String XXX: BuyItemId){
				  BuyItemID=Integer.parseInt(XXX);
				  bddb.setItem_id(BuyItemID);
				  bddb.setBuy_id(buyId);

				  BuyDetailDAO.BuyItemDetail(bddb);
				  //buy_detailテーブルに、アイテムIDを要素の数だけ追加,オートインキーから渡された商品IDを追加
				  System.out.println("商品ID:"+BuyItemID+"を購入します");
				}
			  ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
			  Cart.clear();
			  System.out.println("購入完了したため、カート内の全商品を削除します");

			  /* ====購入完了ページ表示用==== */
			  Buy resultBDB = BuyDAO.getBuyDataBeansByBuyId(buyId);
			  request.setAttribute("resultBDB", resultBDB);

			  ArrayList<Item> buyIDBList = BuyDetailDAO.getItemDataBeansListByBuyId(buyId);
			  request.setAttribute("buyIDBList", buyIDBList);

			  /* ====/購入完了ページ表示用==== */

			  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp");
			  //購入完了ページへ
			  dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}








/*
		HttpSession session = request.getSession();
		try {

			// セッションからカート情報を取得
			ArrayList<Item> Cart = (ArrayList<Item>) Item.cutSessionAttribute(session, "Cart");

			Buy bdb = (Buy) Item.cutSessionAttribute(session, "bdb");

			// 購入情報を登録
			int buyId = BuyDAO.insertBuy(bdb);
			// 購入詳細情報を購入情報IDに紐づけして登録
			for (Item cartInItem : Cart) {
				BuyDetail bddb = new BuyDetail();
				bddb.setBuy_id(buyId);
				bddb.setItem_id(cartInItem.getId());
				BuyDetailDAO.insertBuyDetail(bddb);
			}


			//購入完了ページ表示用
			Buy resultBDB = BuyDAO.getBuyDataBeansByBuyId(buyId);
			request.setAttribute("resultBDB", resultBDB);

			// 購入アイテム情報
			ArrayList<Item> buyIDBList = BuyDetailDAO.getItemDataBeansListByBuyId(buyId);
			request.setAttribute("buyIDBList", buyIDBList);

			// 購入完了ページ
			//request.getRequestDispatcher(EcHelper.BUY_RESULT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
*/