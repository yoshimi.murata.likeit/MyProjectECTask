package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
		dispatcher.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String createLoginId = request.getParameter("createLoginId");
		String createPassword = request.getParameter("createLoginPass");
		String createPassword2=request.getParameter("confirmLoginPass");
		String createName=request.getParameter("createUserName");

		System.out.println(createLoginId);

		UserDAO userDao = new UserDAO();
		User user=userDao.DuplicateCheck(createLoginId);

		if((!(user==null))) {
			//すでに登録されているログインIDが入力された場合の処理
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(!(createPassword.equals(createPassword2))) {
			//createPasswordとcreatePassword2が一致しない場合の処理
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
			dispatcher.forward(request, response);
		}else if(createLoginId.isEmpty()||createPassword.isEmpty()||createPassword2.isEmpty()||createName.isEmpty()) {
			//入力フォームに未入力の項目があった場合
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		userDao.UserCreate(createName,createLoginId,createPassword);
		response.sendRedirect("UserListServlet");
		//登録成功時リダイレクト
	}
}
