package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;

/**
 * 買い物カートから商品を削除する
 */
@WebServlet("/ItemDeleteServlet")
public class ItemDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			String[] DeleteItemIdList = request.getParameterValues("DeleteItemIdList");
			ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
		    if (DeleteItemIdList != null){
		    	for (String deleteItemId : DeleteItemIdList) {
					for (Item cartInItem : Cart) {
						if (cartInItem.getId() == Integer.parseInt(deleteItemId)) {
							Cart.remove(cartInItem);
							break;
						}
					}
				}
		    }

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
		/*
		try {

			//カートを取得
			ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
			//ArrayList<Item> Cart =new ArrayList<Item>();

			//セッションにカートがない場合カートを作成
			if (Cart == null) {
				Cart = new ArrayList<Item>();
			}
			Cart.clear();
			System.out.println("カート内の全商品を削除します");
			session.setAttribute("Cart", Cart);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
*/

