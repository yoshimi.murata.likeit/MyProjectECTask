package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDAO;
import dao.ItemTrackListDAO;
import model.Item;
import model.ItemTrackList;
import model.User;

/**
 * Servlet implementation class BuyItemDetailServlet
 */
@WebServlet("/BuyItemDetailServlet")
public class BuyItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyItemDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		String ItemId=request.getParameter("id");
		System.out.println("商品ID:"+ItemId);
		//URLから商品IDを受け取る

		ItemDAO itemdao=new ItemDAO();
		Item itemDetail=itemdao.findByItem(ItemId);
		//商品IDをアイテム検索メソッドへ渡す

		ItemTrackListDAO ITLdao=new ItemTrackListDAO();
		List<ItemTrackList> ITL=ITLdao.findByTrackList(ItemId);
		//商品IDをアイテムトラックリストメソッドへ渡す


		request.setAttribute("itemDetail", itemDetail);
		//商品IDに紐づけた商品詳細を渡す
		request.setAttribute("ITL", ITL);
		//商品IDに紐づけしたアイテムトラックリストを渡す
		request.setAttribute("ItemId", ItemId);
		//商品IDを渡す


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);
	}
}
