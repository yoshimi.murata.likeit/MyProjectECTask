package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UserUpdataServlet
 */
@WebServlet("/UserUpdataServlet")
public class UserUpdataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		//User情報テーブル、IDに紐づけ
		String UserID=request.getParameter("id");
		System.out.println(UserID);

		UserDAO userdao=new UserDAO();
		User user=userdao.findByUSER(UserID);
		System.out.println(user);
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ユーザー編集機能
		//formでnameに指定したリクエストパラメータを取得する
		request.setCharacterEncoding("UTF-8");
		String updatePass=request.getParameter("UpdatePass");
		String updatePass2=request.getParameter("UpdatePass2");
		String updateName=request.getParameter("UpdateName");
		String id = request.getParameter("id");

		if(!(updatePass.equals(updatePass2))) {
			//PasswordとPassword2が一致しなかった場合の処理
			//errMsg出力してuserUpdate.jspにフォワード
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(updateName.isEmpty()) {
			//入力フォームにパスワード以外の未入力の項目があった場合
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdata.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(updatePass.isEmpty() && updatePass2.isEmpty()){
			//パスワード、パスワード確認両方が空欄なら、それ以外を更新する処理
			UserDAO userdao=new UserDAO();
			userdao.UpdateUser2(id,updateName);
			response.sendRedirect("UserListServlet");
			//パスワード以外を更新するメソッドにパスワード以外の取得した項目を渡す。
			return;
		}
		UserDAO userdao=new UserDAO();
		userdao.UpdateUser(id, updatePass, updateName);

		response.sendRedirect("UserListServlet");
	}
}
