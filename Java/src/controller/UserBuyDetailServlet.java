package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDAO;
import model.Buy;
import model.User;

/**
 * Servlet implementation class UserBuyDetailServlet
 */
@WebServlet("/UserBuyDetailServlet")
public class UserBuyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>
		//URLからUser.idを取得
		//User情報テーブル、IDに紐づけ
		//int UserID=Integer.parseInt(request.getParameter("id"));
		//System.out.println(UserID);

		//Buy UserIdInfoBuyList=BuyDAO.getBuyDataBeansByUserId(UserID);
		//request.setAttribute("UserIdInfoBuyList", UserIdInfoBuyList);
		request.setCharacterEncoding("UTF-8");
		int UserID=Integer.parseInt(request.getParameter("id"));
		try {
			List<Buy> UserIdInfoBuyList=BuyDAO.getByBuyUserId(UserID);

			request.setAttribute("UserIdInfoBuyList",UserIdInfoBuyList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserBuyDetail.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}
