package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;
import model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがある場合、ユーザ画面に遷移させる
		HttpSession session = request.getSession();
		User XXX=(User)session.getAttribute("userInfo");
		if(!(XXX==null)) {
			System.out.println("ログインセッション有");
			response.sendRedirect("UserListServlet");
			return;
		}
		// </TODO>
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId=request.getParameter("loginId");
		String password=request.getParameter("password");
		System.out.println("入力されたログインID:"+loginId);
		System.out.println("入力されたパスワード"+password);

		UserDAO userdao=new UserDAO();
		User user=userdao.findByLoginInfo(loginId, password);

		User UserData=userdao.findByUserLoginId(loginId);

		if (user == null) {
			request.setAttribute("errMsg", "入力されたログインIDとパスワードが一致しません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}


		/** テーブルに該当のデータが見つかった場合 **/
		System.out.println("入力されたログインIDとパスワードが一致しました");
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		session.setAttribute("UserData", UserData);

		if(user.getLogin_id().equals("admin")) {
			response.sendRedirect("UserListServlet");
			return;
		}else {
			response.sendRedirect("IndexServlet");
		}
		//ログイン時、管理者はユーザーリストへ、それ以外のユーザーはTOPページへリダイレクト
	}
}
