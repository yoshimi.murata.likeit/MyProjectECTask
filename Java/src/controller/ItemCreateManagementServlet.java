package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.ItemDAO;
import dao.ItemTrackListDAO;
import model.ItemTrackList;

/**
 * Servlet implementation class ItemCreateManagementServlet
 */
@WebServlet("/ItemCreateManagementServlet")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-RJR9497.001\\Documents\\MyProjectECTask\\Java\\WebContent\\img", maxFileSize=1048576)
public class ItemCreateManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemCreateManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ItemCreateManagement.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String ItemName=request.getParameter("CreateItemByName");
		String ItemArtistName=request.getParameter("ItemByArtistName");
		String ItemText=request.getParameter("CreateItemByText");
		int ItemPrice=Integer.parseInt(request.getParameter("CreateItemByPrice"));
		//String ItemPrice=request.getParameter("CreateItemByPrice");
		Part part = request.getPart("CreateItemByFileName");
		String ItemFileName=this.getFileName(part);
		part.write(ItemFileName);
		String ItemURL=request.getParameter("CreateItemByURL");

		int i = ItemDAO.CreateByItem(ItemName,ItemArtistName,ItemText,ItemPrice,ItemFileName,ItemURL);
		//

		//String CreateItemByAlbumId = request.getParameter("CreateItemByAlbumId");
		//int CreateItemByAlbumId = Integer.parseInt(request.getParameter("CreateItemByAlbumId"));


		String[] CreateItemByTrackName = request.getParameterValues("ByFileTrackList");
		ItemTrackList itl=new ItemTrackList();
		int a = 1;

		  for (String XXX: CreateItemByTrackName){
			  if(XXX.isEmpty()) {
				  continue;
			  }

				  itl.setTrack_name(XXX);
				  itl.setAlbum_id(i);
				  itl.setTrack_id(a);
				  a++;
				  ItemTrackListDAO.CreateByItemTrackList(itl);

		}


		response.sendRedirect("UserManagementServlet");

		//RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserManagement.jsp");
		//dispatcher.forward(request, response);

	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
