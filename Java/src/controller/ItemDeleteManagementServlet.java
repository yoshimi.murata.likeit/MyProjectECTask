package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ItemDAO;
import dao.ItemTrackListDAO;
import model.Item;
import model.User;

/**
 * Servlet implementation class ItemDeleteManagementServlet
 */
@WebServlet("/ItemDeleteManagementServlet")
public class ItemDeleteManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDeleteManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		String ItemId=request.getParameter("id");
		//URLから商品IDを受け取る

		ItemDAO itemdao=new ItemDAO();
		Item itemDetail=itemdao.findByItem(ItemId);
		//商品IDをアイテム検索メソッドへ渡す

		request.setAttribute("itemDetail", itemDetail);
		//商品IDに紐づけた商品詳細を渡す
		request.setAttribute("ItemId", ItemId);
		//商品IDを渡す


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ItemDeleteManagement.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO:アイテム削除
		//String DeleteItemId=request.getParameter("id");
		int DeleteItemId=Integer.parseInt(request.getParameter("id"));
		System.out.println("商品ID:"+DeleteItemId+"をテーブルから削除します");

		ItemDAO.DeleteByItem(DeleteItemId);
		ItemTrackListDAO.DeleteByItemTrackList(DeleteItemId);


		response.sendRedirect("UserManagementServlet");
		//RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserManagement.jsp");
		//dispatcher.forward(request, response);
	}
}
