package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;

/**
 * 買い物カートに商品追加画面
 *
 */
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		try {
			//カートを取得
			ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
			//ArrayList<Item> Cart =new ArrayList<Item>();

			//セッションにカートがない場合カートを作成
			if (Cart == null) {
				Cart = new ArrayList<Item>();
			}
			session.setAttribute("Cart", Cart);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO: handle exception
	}
}
}
/*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			String ItemId=request.getParameter("ItemId");
			System.out.println("商品ID:"+ItemId+"をカートに追加します");
			ItemDAO itemdao=new ItemDAO();
			Item ITEM=itemdao.findByItem(ItemId);
			//カートを取得
			ArrayList<Item> Cart = (ArrayList<Item>) session.getAttribute("Cart");
			//ArrayList<Item> Cart =new ArrayList<Item>();

			//セッションにカートがない場合カートを作成
			if (Cart == null) {
				Cart = new ArrayList<Item>();
			}
			Cart.add(ITEM);
			session.setAttribute("Cart", Cart);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
*/