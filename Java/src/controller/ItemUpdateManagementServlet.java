package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.ItemDAO;
import dao.ItemTrackListDAO;
import model.Item;
import model.ItemTrackList;
import model.User;

/**
 * Servlet implementation class ItemUpdateManagementServlet
 */
@WebServlet("/ItemUpdateManagementServlet")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-RJR9497.001\\Documents\\MyProjectECTask\\Java\\WebContent\\img", maxFileSize=1048576)
public class ItemUpdateManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemUpdateManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		String ItemID=request.getParameter("id");

		ItemDAO itemdao=new ItemDAO();
		Item ItemDetail= itemdao.findByItem(ItemID);
		request.setAttribute("ItemDetail",ItemDetail);
		//商品IDをアイテムメソッドへ渡す

		ItemTrackListDAO ITLdao=new ItemTrackListDAO();
		List<ItemTrackList> ItemTrackList=ITLdao.findByTrackList(ItemID);
		request.setAttribute("ItemTrackList",ItemTrackList);
		//商品IDをアイテムトラックリストメソッドへ渡す

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ItemUpdateManagement.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//String UpdateItemById=request.getParameter("UpdateItemById");
		int UpdateItemById=Integer.parseInt(request.getParameter("UpdateItemById"));
		String UpdateItemName=request.getParameter("UpdateItemByName");
		//String UpdateItemFileName=request.getParameter("UpdateItemByFileName");
		String UpdateItemArtistName=request.getParameter("UpdateItemByArtistName");
		String UpdateItemDetailText=request.getParameter("UpdateItemByDetailText");
		int UpdateItemPrice=Integer.parseInt(request.getParameter("UpdateItemByPrice"));
		String UpdateItemURL=request.getParameter("UpdateItemByURL");

		//画像ファイル
		Part part = request.getPart("UpdateItemByFileName");
		String UpdateItemFileName=this.getFileName(part);
		part.write(UpdateItemFileName);

		ItemDAO.UpdateItem(UpdateItemById, UpdateItemName, UpdateItemFileName, UpdateItemArtistName,
				UpdateItemDetailText, UpdateItemPrice, UpdateItemURL);

		//TODO:TrackList
		String[] UpdateTrackList = request.getParameterValues("UpdateTrackList");

		ItemTrackList itl=new ItemTrackList();
		int a = 1;
		  for (String XXX: UpdateTrackList){
			  if(XXX.isEmpty()) {
				  continue;
			  }

				  itl.setTrack_name(XXX);
				  itl.setAlbum_id(UpdateItemById);
				  itl.setTrack_id(a);
				  a++;
				  ItemTrackListDAO.UpdateByItemTrackList(itl);

		}

		response.sendRedirect("UserManagementServlet");

		//RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserManagement.jsp");
		//dispatcher.forward(request, response);



	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }

}
