package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDAO;
import dao.BuyDetailDAO;
import model.Buy;
import model.Item;
import model.User;

/**
 * Servlet implementation class UserBuyHistoryDetailServlet
 */
@WebServlet("/UserBuyHistoryDetailServlet")
public class UserBuyHistoryDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistoryDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		request.setCharacterEncoding("UTF-8");
		int BuyID=Integer.parseInt(request.getParameter("buyid"));
		System.out.println("buy.id:"+BuyID);

		try {
			Buy BDBBB=BuyDAO.getBuyDataBeansByBuyId(BuyID);
			ArrayList<Item> IDBLB=BuyDetailDAO.getItemDataBeansListByBuyId(BuyID);
			request.setAttribute("BDBBB",BDBBB);
			request.setAttribute("IDBLB",IDBLB);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserBuyHistoryDetail.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
