package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import model.ItemTrackList;

public class ItemTrackListDAO {


	/**
	 * 商品IDに紐づけしたアイテムトラック情報を取得する
	 * @return
	 */
	public List<ItemTrackList> findByTrackList(String itemid) {
		Connection conn = null;
		List<ItemTrackList> itemList = new ArrayList<ItemTrackList>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT album_id,track_id,track_name FROM item INNER JOIN item_tracklist ON item.id = item_tracklist.album_id WHERE album_id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1, itemid);
			ResultSet RS = pSTMt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (RS.next()) {
				int albumID = RS.getInt("album_id");
				int trackID = RS.getInt("track_id");
				String trackName = RS.getString("track_name");
				ItemTrackList ITL= new ItemTrackList(albumID,trackID,trackName);

				itemList.add(ITL);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}


	public static void CreateByItemTrackList(ItemTrackList itemTrack) {
		//int AlbumID,int TrackId,String TrackName
		/**
		 * 詳細情報を登録
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO item_tracklist(album_id,track_id,track_name) "
					+ "VALUES (?,?,?)";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setInt(1,itemTrack.getAlbum_id());
			pSTMt.setInt(2,itemTrack.getTrack_id());
			pSTMt.setString(3, itemTrack.getTrack_name());
			pSTMt.executeUpdate();
			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}
	public static void UpdateByItemTrackList(ItemTrackList itemTrack) {
		//int AlbumID,int TrackId,String TrackName
		/**
		 * 詳細情報を更新
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE item_tracklist SET track_name=? WHERE track_id=? AND album_id=?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1,itemTrack.getTrack_name());
			pSTMt.setInt(2,itemTrack.getTrack_id());
			pSTMt.setInt(3, itemTrack.getAlbum_id());
			pSTMt.executeUpdate();
			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}

	public static void DeleteByItemTrackList(int ItemID) {
		/**
		 * IDに紐づけしたアイテム情報を削除
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "DELETE FROM item_tracklist WHERE album_id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setInt(1, ItemID);
			pSTMt.executeUpdate();
			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
		return;
	}
}
