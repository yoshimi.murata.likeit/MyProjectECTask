package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import model.Buy;

public class BuyDAO {
	public static int BuyItem(String BuyUserId,String TotalPrice) {
		/**
		 * buyテーブルに、入力された情報を追加=購入
		 * @return
		 */
		Connection conn = null;
		int autoIncKey = -1;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO buy(user_id,total_price,create_date) VALUES(?,?,NOW())";



			// INSERTを実行
			PreparedStatement PStmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			PStmt.setString(1, BuyUserId);
			PStmt.setString(2, TotalPrice);
			PStmt.executeUpdate();

			ResultSet rs = PStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}


			PStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return autoIncKey;
	}


	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static Buy getBuyDataBeansByBuyId(int buyId)  {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy WHERE buy.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			Buy bdb = new Buy();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setTotal_price(rs.getInt("total_price"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));
				bdb.setUser_id(rs.getInt("user_id"));
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

			return bdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (con != null) {
			}
		}
		return null;
	}

	/**
	 * User.idによる購入情報検索
	 * @param user.Id
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static List<Buy> getByBuyUserId(int userId) throws SQLException {
		Connection con = null;
		List<Buy> BuyDataByUserId = new ArrayList<Buy>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy WHERE user_id = ? ORDER BY create_date DESC;");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				Buy bdbuid = new Buy();
				bdbuid.setId(rs.getInt("id"));
				bdbuid.setUser_id(rs.getInt("user_id"));
				bdbuid.setTotal_price(rs.getInt("total_price"));
				bdbuid.setCreate_date(rs.getTimestamp("create_date"));

				//ここにインスタンスをつくるとループの度にリセットされる！
				BuyDataByUserId.add(bdbuid);
			}

			System.out.println("searching Buy by user.id has been completed");

			return BuyDataByUserId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



}
