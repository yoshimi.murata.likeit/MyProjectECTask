package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import model.BuyDetail;
import model.Item;

public class BuyDetailDAO {
	public static void BuyItemDetail(BuyDetail bddb) {
		/**
		 * buy_detailテーブルに、入力された情報を追加=購入
		 * @return
		 */
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)";

			// INSERTを実行
			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, bddb.getBuy_id());
			PStmt.setInt(2, bddb.getItem_id());
			PStmt.executeUpdate();
			PStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return ;
	}

	 /**
     * 購入IDによる購入詳細情報検索
     * @param buyId
     * @return buyDetailItemList ArrayList<ItemDataBeans>
     *             購入詳細情報のデータを持つJavaBeansのリスト
     * @throws SQLException
     */
	public static ArrayList<Item> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT item.id,"
					+ " item.name,"
					+ " item.price,"
					+ " item.file_name,"
					+ " item.detail_text"
					+ " FROM buy_detail"
					+ " JOIN item"
					+ " ON buy_detail.item_id = item.id"
					+ " WHERE buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<Item> buyDetailItemList = new ArrayList<Item>();

			while (rs.next()) {
				Item idb = new Item();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));
				idb.setFile_name(rs.getString("file_name"));
				idb.setDetail_text(rs.getString("detail_text"));


				buyDetailItemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}

