package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import model.User;


public class UserDAO {


	/**
	 * ログイン
	 * @return:User
	 */
	public User findByLoginInfo(String loginId, String password) {
		/**
		 * ログインIDとパスワードに紐づくユーザ情報を返す
		 * @param loginId
		 * @param password
		 * @return
		 */
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";

			//メモ；ここに暗号化の処理を書く
			String encryptionPass = encryption(password);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, encryptionPass);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			//int ID = rs.getInt("id");
			//ID追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			//return new User(ID,loginIdData, nameData);
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	//
	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// 管理者以外を取得する
			String sql = "SELECT * FROM user WHERE id!=1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String loginPassword = rs.getString("login_password");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");
				User user = new User(id,name,loginId,loginPassword,createDate,updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}



	public void UserCreate(String CreateName,String CreateLoginId,String CreateLoginPass) {
		/**
		 * UserCreateServlet
		 * ユーザー新規作成
		 * @return
		 */
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO user(name,login_id,login_password,create_date,update_date)VALUES (?,?,?,NOW(),NOW())";
			//メモ；ここに暗号化の処理を書く
			String encryptionPass = encryption(CreateLoginPass);

			// INSERTを実行
			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, CreateName);
			PStmt.setString(2, CreateLoginId);
			PStmt.setString(3, encryptionPass);
			PStmt.executeUpdate();
			PStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return ;
	}

	public User DuplicateCheck(String createID) {
		/**
		 * ログインIDの重複確認
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";
			//login_id検索してrsに

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, createID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				//false 重複無
				return null;
			}
			//true 重複あり。インスタンスを返す
			//getString("カラム名")
			String CheckId = rs.getString("login_id");
			return new User(CheckId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String encryption(String encryptionPass) {
		/**
		 * パスワードの暗号化処理
		 * @return
		 */
		//暗号化メソッド
		//ハッシュを生成したい元の文字列
		String source = encryptionPass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;

	}



	public void UpdateUser(String id, String updatePass, String updateName) {
		/**
		 * UserUpdataServlet
		 * ユーザー情報更新
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE user SET login_password=?,name=? WHERE id=?";

			//TODO:暗号化処理
			String encryptionPass = encryption(updatePass);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, encryptionPass);
			pStmt.setString(2, updateName);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public void UpdateUser2(String id, String updateName) {
		/**
		 * UserUpdataServlet
		 * ユーザー情報更新(パスワード以外)
		 * @return
		 */
			Connection conn = null;
			try {
				conn = DBManager.getConnection();
				String sql = "UPDATE user SET name=? WHERE id=?";
				//String encryptionPass = encryption(updatePassword);
				//パスワードは更新しないので暗号化処理なし
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, updateName);
				pStmt.setString(2, id);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}




	public User findByUSER(String id) {
		/**
		 * IDに紐づけしたユーザー情報を検索
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1, id);

			ResultSet rS = pSTMt.executeQuery();

			if (!rS.next()) {
				return null;
			}
			int Id = rS.getInt("id");
			String name = rS.getString("name");
			String loginId = rS.getString("login_id");
			Date createDate = rS.getDate("create_date");
			Date updateDate = rS.getDate("update_date");
			return new User(Id,name,loginId,createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public User findByUserLoginId(String LoginId) {
		/**
		 * ログインIDに紐づけしたユーザー情報を検索
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1, LoginId);

			ResultSet rS = pSTMt.executeQuery();

			if (!rS.next()) {
				return null;
			}
			int Id = rS.getInt("id");
			String name = rS.getString("name");
			String loginId = rS.getString("login_id");
			Date createDate = rS.getDate("create_date");
			Date updateDate = rS.getDate("update_date");
			return new User(Id,name,loginId,createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザー検索
	 * @return
	 */
	public List<User> findResearch(String loginIdP, String nameP, String startCreateDate, String endCreateDate) {
		//ユーザー検索機能
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id!=1";
			//管理者以外をすべて取得

			//"SELECT * FROM user where login_id=? and name LIKE %?% and (birth_date>=? AND birth_date<=?)";
			//"SELECT * FROM user WHERE id!=1 AND login_id={?(loginIdP)} AND name LIKE %{?(nameP)}% AND (birth_date>=startDate AND birth_date<=endDate)

/*
			if(!(loginIdP.equals(""))) {
				//入力されたログインIDが空欄ではない場合
				//sql += " AND login_id = '" + loginIdP + "'";
				sql += " AND login_id = '" + loginIdP + "AND name LIKE '" + "%'"+ nameP +"%'" + "AND (birth_date>='" + startDate + "AND birth_date<= '" + endDate + ")'" ;
			}else if(loginIdP.equals("")) {
				//ログインIDが空欄の場合
				sql += "AND name LIKE '" + "%'" + nameP + "%'" + "AND (birth_date>='" + startDate + "AND birth_date<= '" + endDate + ")'" ;
			}
*/

			//IFの分岐は4つ
			//それぞれにsql文を加算する
			if(!(loginIdP.isEmpty())) {
				sql += " AND login_id = '" + loginIdP + "'" ;
			}
			if(!(nameP.isEmpty())) {
				sql += " AND name LIKE '" + "%" + nameP + "%'";
			}
			if(!(startCreateDate.isEmpty())) {
				sql+= " AND create_date>='" + startCreateDate + "'" ;
			}
			if(!(endCreateDate.isEmpty())) {
				sql+= " AND create_date<= '" + endCreateDate + "'" ;
			}


			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			//PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date CreateDateS = rs.getDate("create_date");
				Date CreateDateE = rs.getDate("create_date");
				User user = new User(loginId, name, CreateDateS,CreateDateE);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void deleteByUser(String id) {
		//メモ：戻り値を返す必要はない
		//UserDereteServlet
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "DELETE FROM user WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}