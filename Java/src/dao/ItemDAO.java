package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import model.Item;

public class ItemDAO {

	/**
	 * 全てのアイテム情報を取得する
	 * @return
	 */
	public List<Item> findAll() {
		Connection conn = null;
		List<Item> itemList = new ArrayList<Item>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM item ORDER BY id DESC";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String artistName = rs.getString("artist_name");
				String detailText = rs.getString("detail_text");
				int price  = rs.getInt("price");
				String fileName = rs.getString("file_name");
				Item item = new Item(id,name,artistName,detailText,price,fileName);

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<Item> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM item ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<Item> RanditemList = new ArrayList<Item>();

			while (rs.next()) {
				Item item = new Item();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setArtist_name(rs.getString("artist_name"));
				item.setDetail_text(rs.getString("detail_text"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));
				RanditemList.add(item);
			}
			System.out.println("getAllItem completed");
			return RanditemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public Item findByItem(String Itemid) {
		/**
		 * IDに紐づけしたアイテム情報を検索
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM item WHERE id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1, Itemid);

			ResultSet rS = pSTMt.executeQuery();

			Item item = new Item();

			if (!rS.next()) {
				return null;
			}
			item.setId(rS.getInt("id"));
			//int ID=rS.getInt("id");

			//String name = rS.getString("name");
			item.setName(rS.getString("name"));

			//String artistName= rS.getString("artist_name");
			item.setArtist_name(rS.getString("artist_name"));

			//String detailText= rS.getString("detail_text");
			item.setDetail_text(rS.getString("detail_text"));

			//int price = rS.getInt("price");
			item.setPrice(rS.getInt("price"));

			//String fileName=rS.getString("file_name");
			item.setFile_name(rS.getString("file_name"));

			//String youtubeURL=rS.getString("youtube_url");
			item.setYoutube_url(rS.getString("youtube_url"));

			//return new Item(ID,name,artistName,detailText,price,fileName,youtubeURL);
			return item;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public static Item DeleteByItem(int ItemID) {
		/**
		 * IDに紐づけしたアイテム情報を削除
		 * @return
		 */
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "DELETE FROM item WHERE id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setInt(1, ItemID);
			pSTMt.executeUpdate();
			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public static int CreateByItem(String ItemName,String ItemArtistName,String ItemText,int ItemPrice,
			String ItemFileName,String ItemURL) {
		/**
		 * アイテム情報を登録
		 * @return
		 */
		Connection conn = null;
		int autoIncKey = -1;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO item(name,artist_name,detail_text,price,file_name,youtube_url) "
					+ "VALUES (?,?,?,?,?,?)";
			PreparedStatement pSTMt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pSTMt.setString(1, ItemName);
			pSTMt.setString(2, ItemArtistName);
			pSTMt.setString(3, ItemText);
			pSTMt.setInt(4, ItemPrice);
			pSTMt.setString(5, ItemFileName);
			pSTMt.setString(6, ItemURL);

			pSTMt.executeUpdate();

			ResultSet rs = pSTMt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			pSTMt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return autoIncKey;
	}

	/**
	 * 商品情報更新
	 * @return
	 */
	public static Item UpdateItem(int UpdateItemId,String UpdateItemName, String UpdateItemFileName,
			String UpdateItemArtistName,
			String UpdateItemDetailText,int UpdateItemPrice,String UpdateItemURL) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE item SET "
					+ "name=?,artist_name=?,"
					+ "detail_text=?,price=?,file_name=?,youtube_url=? WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,UpdateItemName);
			pStmt.setString(2,UpdateItemArtistName);
			pStmt.setString(3,UpdateItemDetailText);
			pStmt.setInt(4,UpdateItemPrice);
			pStmt.setString(5,UpdateItemFileName);
			pStmt.setString(6,UpdateItemURL);

			pStmt.setInt(7,UpdateItemId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}


	/**
	 * 商品検索
	 * @return
	 */
	public static List<Item> findSearchItem(String ItemName,String ItemArtistName) {
		//ユーザー検索機能
		Connection conn = null;
		List<Item> ItemList = new ArrayList<Item>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM item ";

			//それぞれにsql文を加算する
			if(!(ItemName.equals(ItemArtistName))) {
				sql += " WHERE name LIKE '" + "%" + ItemName + "%'" + " AND artist_name LIKE '" + "%" + ItemArtistName + "%'";
			}

			else if(!(ItemName.isEmpty())) {
				sql += " WHERE name LIKE '" + "%" + ItemName + "%'";

			}
			else if(!(ItemArtistName.isEmpty())) {
				sql += " WHERE artist_name LIKE '" + "%" + ItemArtistName + "%'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			//PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String Name=rs.getString("name");
				String ArtistName=rs.getString("artist_name");
				String DetailText=rs.getString("detail_text");
				int Price=rs.getInt("price");
				String FileName=rs.getString("file_name");
				String URL=rs.getString("youtube_url");

				Item item=new Item(Name,ArtistName,DetailText,Price,FileName,URL);
				ItemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return ItemList;
	}
}
