package model;

import java.io.Serializable;

public class ItemTrackList implements Serializable {

	private int id;
	private int album_id;
	private int track_id;
	private String track_name;


	public ItemTrackList(int albumID, int trackID, String trackName) {
		// TODO 商品IDに紐づけしたアイテムトラック情報を取得する
		this.album_id=albumID;
		this.track_id=trackID;
		this.track_name=trackName;

	}


	public ItemTrackList() {
		// TODO 自動生成されたコンストラクター・スタブ
	}


	//アクセサ
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAlbum_id() {
		return album_id;
	}

	public void setAlbum_id(int album_id) {
		this.album_id = album_id;
	}

	public int getTrack_id() {
		return track_id;
	}

	public void setTrack_id(int track_id) {
		this.track_id = track_id;
	}

	public String getTrack_name() {
		return track_name;
	}

	public void setTrack_name(String track_name) {
		this.track_name = track_name;
	}


}
