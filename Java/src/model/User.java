package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String name;
	private String login_id;
	private String login_password;
	private Date create_date;
	private Date update_date;

	public User(String loginIdData, String NameData) {
		// TODO ログイン時コンストラクタ
		this.login_id=loginIdData;
		this.name=NameData;
	}

	public User(int id2, String name2, String loginId, String loginPassword, Date createDate,
			Date updateDate) {
		// 全ユーザー情報を検索する
		this.id=id2;
		this.name=name2;
		this.login_id=loginId;
		this.login_password=loginPassword;
		this.create_date=createDate;
		this.update_date=updateDate;
	}


	public User(int id2, String name2, String loginId, Date createDate, Date updateDate) {
		// TODO idに紐づけしたユーザー情報を検索
		this.id=id2;
		this.name=name2;
		this.login_id=loginId;
		this.create_date=createDate;
		this.update_date=updateDate;
	}

	public User(int id,String updatePassData, String updateNameData) {
		//UserUpdateServlet
		this.id=id;
		this.login_password=updatePassData;
		this.name=updateNameData;
	}
	public User(int id,String updateNameData) {
		//UserUpdate2Servlet
		this.id=id;
		this.name=updateNameData;
	}

	public User(int id) {
		//UserDeleteServlet
		this.id=id;
	}
	public User(String loginId, String name2, Date createDateS, Date createDateE) {
		// UserListServlet ログインID・ユーザ名・登録日時に紐づけしたユーザー情報を検索
		this.login_id=loginId;
		this.name=name2;
		this.create_date=createDateS;

	}


	public User(String checkId) {
		// TODO ログインID重複をチェックする
		this.login_id=checkId;
	}

	//アクセサ
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getLogin_password() {
		return login_password;
	}
	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}


}
