package model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

public class Item implements Serializable  {

	private int id;
	private String name;
	private String artist_name;
	private String detail_text;
	private int price;
	private String file_name;
	private String youtube_url;

	public Item(int id,String name,String artistName,String detailText,int price,String fileName) {
		// 全アイテム情報を検索する
		this.id=id;
		this.name=name;
		this.artist_name=artistName;
		this.detail_text=detailText;
		this.price=price;
		this.file_name=fileName;
	}

public Item(int ID,String name2, String artistName, String detailText, int price2, String fileName,
			String youtubeURL) {
		// 商品IDに紐づけしたアイテム情報を検索する
	this.id=ID;
	this.name=name2;
	this.artist_name=artistName;
	this.detail_text=detailText;
	this.price=price2;
	this.file_name=fileName;
	this.youtube_url=youtubeURL;
	}

/**
 * 商品情報追加
 * @return
 */
public Item(String ItemName,String ItemArtistName,String ItemText,int ItemPrice,
		String ItemFileName,String ItemURL) {
	this.name=ItemName;
	this.artist_name=ItemArtistName;
	this.detail_text=ItemText;
	this.price=ItemPrice;
	this.file_name=ItemFileName;
	this.youtube_url=ItemURL;
}

/**
 * IDに紐づけしたアイテム情報を検索
 * @return
 */
public Item() {
}

/**
 * アイテム情報を検索
 * @return
 */
public Item(String ItemName,String ItemArtistName) {
	this.name=ItemName;
	this.artist_name=ItemArtistName;
}





/**
 * IDに紐づけしたアイテム情報を削除
 * @return
 */
public Item(int ItemID) {
	this.id=ItemID;
}

/**
 * 商品の合計金額を算出する
 *
 * @param items
 * @return total
 */
public static int getTotalItemPrice(ArrayList<Item> items) {
	int total = 0;
	for (Item item : items) {
		total += item.getPrice();
	}
	return total;
}

/**
 * セッションから指定データを取得（削除も一緒に行う）
 *
 * @param session
 * @param str
 * @return
 */
public static Object cutSessionAttribute(HttpSession session, String str) {
	Object test = session.getAttribute(str);
	session.removeAttribute(str);

	return test;
}

//アクセサ
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getArtist_name() {
	return artist_name;
}
public void setArtist_name(String artist_name) {
	this.artist_name = artist_name;
}
public String getDetail_text() {
	return detail_text;
}
public void setDetail_text(String detail_text) {
	this.detail_text = detail_text;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getFile_name() {
	return file_name;
}
public void setFile_name(String file_name) {
	this.file_name = file_name;
}
public String getYoutube_url() {
	return youtube_url;
}
public void setYoutube_url(String youtube_url) {
	this.youtube_url = youtube_url;
}

}