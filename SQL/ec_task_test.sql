USE ec_task;
SELECT * FROM user WHERE id = 2;
SELECT * FROM user WHERE id!=1;
DROP TABLE user;
SELECT *FROM user;
INSERT INTO user(name,login_id,login_password,create_date,update_date)
VALUES ('test2','test2','test2','2001-01-01','2001-01-01');

"INSERT INTO user(name,login_id,login_password,create_date,update_date)VALUES (?,?,?,NOW(),NOW())";

CREATE TABLE user (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  name varchar(256) DEFAULT NULL,
  login_id varchar(256) DEFAULT NULL,
  login_password varchar(256) DEFAULT NULL,
  create_date date DEFAULT NULL,
  update_date date DEFAULT NULL
);

INSERT INTO user (name,login_id,login_password,create_date,update_date)
VALUES (1,'admin','admin','password','2020-01-01','2020-01-01');





CREATE TABLE item(
id INT(11) PRIMARY KEY AUTO_INCREMENT,
name varchar(256) DEFAULT NULL, 
artist_name varchar(256) DEFAULT NULL,
detail_text varchar(512) DEFAULT NULL,
price int(11) DEFAULT NULL,
file_name varchar(256) DEFAULT NULL,
youtube_url varchar(256) DEFAULT NULL
);
USE ec_task;
INSERT INTO item(id,name,artist_name,detail_text,price,file_name,youtube_url) VALUES
(1,'Freak Out!','Frank Zappa','���v���^���� | 01:00 Released June 27,1966',2269,'freakout.jpeg','https://www.youtube.com/embed/yEbjInR6irI');

INSERT INTO item(id,name,artist_name,detail_text,price,file_name,youtube_url) VALUES
(2,'Absolutely Free','Frank Zappa','���^���� | 44:00 Released May 26,1967',1631,'FrankZappa-AbsolutelyFree.jpg','https://www.youtube.com/embed/oNULHGsy3SE');

SELECT * FROM item WHERE id=1;


UPDATE item SET file_name ='71Tf9evpABL._AC_SX466_.jpg' WHERE id=1;



USE ec_task;

CREATE TABLE item_tracklist(
id  int(11) PRIMARY KEY AUTO_INCREMENT,
album_id int(11) DEFAULT NULL, 
track_id int(11) DEFAULT NULL,
track_name varchar(256) DEFAULT NULL); 

INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(1,1,'Hungry Freaks, Daddy'),
(1,2,'I Ain''t Got No Heart'),
(1,3,'Who Are the Brain Police?'),
(1,4,'Go Cry on Somebody Elses Shoulder'),
(1,5,'Motherly Love'),
(1,6,'How Could I Be Such a Fool'),
(1,7,'Wowie Zowie'),
(1,8,'You Didnt Try to Call Me'),
(1,9,'Any Way the Wind Blows'),
(1,10,'I''m Not Satisfied'),
(1,11,'You''re Probably Wondering Why I''m Here'),
(1,12,'Trouble Every Day'),
(1,13,'Help, I''m a Rock '),
(1,14,'The Return of the Son of Monster Magnet '),

(2,1,'Plastic People'),
(2,2,'The Duke of Prunes'),
(2,3,'Amnesia Vivace'),
(2,4,'The Duke Regains His Chops'),
(2,5,'Call Any Vegetable'),
(2,6,'Invocation & Ritual Dance of the Young Pumpkin'),
(2,7,'Soft-Sell Conclusion'),
(2,8,'America Drinks'),
(2,9,'Status Back Baby'),
(2,10,'Uncle Bernie''s Farm'),
(2,11,'Son of Suzy Creamcheese'),
(2,12,'Brown Shoes Don''t Make It'),
(2,13,'America Drinks & Goes Home');

