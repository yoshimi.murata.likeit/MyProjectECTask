USE ec_task;

CREATE TABLE buy_detail (
id int(11) PRIMARY KEY AUTO_INCREMENT,
buy_id int(11) DEFAULT NULL,
item_id int(11) DEFAULT NULL);

SELECT item.id,
item.name,
item.price
FROM buy_detail
JOIN item
ON buy_detail.item_id = item.id
WHERE buy_detail.buy_id = 13;

SELECT * FROM buy_detail
JOIN item
ON buy_detail.item_id = item.id
WHERE buy_detail.buy_id = 24;

SELECT item.id,
item.name,
item.price,
item.file_name,
item.detail_text
FROM buy_detail
JOIN item
ON buy_detail.item_id = item.id
WHERE buy_detail.buy_id = ?;


SELECT item.name,item.price,item.detail_text,item.file_name
FROM 
item JOIN buy_detail
ON buy_detail.item_id=item.id
WHERE buy_detail.buy_id=32;

SELECT * FROM buy_detail WHERE buy_id = 32;
