USE ec_task;
CREATE TABLE item_tracklist(
id int(11) PRIMARY KEY AUTO_INCREMENT,
album_id int(11) DEFAULT NULL, 
track_id int(11) DEFAULT NULL,
track_name varchar(256) DEFAULT NULL); 

SELECT album_id,track_id,track_name FROM
item INNER JOIN item_tracklist 
ON item.id = item_tracklist.album_id
WHERE album_id = 1;

UPDATE item_tracklist SET track_name='gorillaBanana' WHERE album_id=43 AND track_id=2;

DELETE FROM WHERE ? BETWEEN ? AND ?;

INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(1,1,'Hungry Freaks, Daddy'),
(1,2,'I Ain''t Got No Heart'),
(1,3,'Who Are the Brain Police?'),
(1,4,'Go Cry on Somebody Elses Shoulder'),
(1,5,'Motherly Love'),
(1,6,'How Could I Be Such a Fool'),
(1,7,'Wowie Zowie'),
(1,8,'You Didnt Try to Call Me'),
(1,9,'Any Way the Wind Blows'),
(1,10,'I''m Not Satisfied'),
(1,11,'You''re Probably Wondering Why I''m Here'),
(1,12,'Trouble Every Day'),
(1,13,'Help, I''m a Rock '),
(1,14,'The Return of the Son of Monster Magnet '),
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(2,1,'Plastic People'),
(2,2,'The Duke of Prunes'),
(2,3,'Amnesia Vivace'),
(2,4,'The Duke Regains His Chops'),
(2,5,'Call Any Vegetable'),
(2,6,'Invocation & Ritual Dance of the Young Pumpkin'),
(2,7,'Soft-Sell Conclusion'),
(2,8,'America Drinks'),
(2,9,'Status Back Baby'),
(2,10,'Uncle Bernie''s Farm'),
(2,11,'Son of Suzy Creamcheese'),
(2,12,'Brown Shoes Don''t Make It'),
(2,13,'America Drinks & Goes Home');
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(3,1,'part one'),
(3,2,'part two');
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(4,1,'Are You Hung Up?'),
(4,2,'Who Needs the Peace Corps?'),
(4,3,'Concentration Moon'),
(4,4,'Mom & Dad'),
(4,5,'Telephone Conversation'),
(4,6,'Bow Tie Daddy'),
(4,7,'Harry, You''re a Beast'),
(4,8,'What''s the Ugliest Part of Your Body?'),
(4,9,'Absolutely Free'),
(4,10,'Flower Punk'),
(4,11,'Hot Poop'),
(4,12,'Nasal Retentive Calliope Music'),
(4,13,'Let''s Make the Water Turn Black'),
(4,14,'The Idiot Bastard Son'),
(4,15,'Lonely Little Girl'),
(4,16,'Take Your Clothes Off When You Dance'),
(4,17,'Let''s Make the Water Turn Black"'),
(4,18,'What''s the Ugliest Part of Your Body? (Reprise)'),
(4,19,'Mother People'),
(4,20,'The Chrome Plated Megaphone of Destiny');
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(5,1,'Cheap Thrills'),
(5,2,'Love of My Life'),
(5,3,'Deseri'),
(5,4,'I''m Not Satisfied'),
(5,5,'Jelly Roll Gum Drop'),
(5,6,'Anything'),
(5,7,'Later That Night'),
(5,8,'You Didn''t Try to Call Me'),
(5,9,'Fountain of Love'),
(5,10,'No. No. No.'),
(5,11,'Hot Poop'),
(5,12,'Any Way the Wind Blows'),
(5,13,'Stuff Up the Cracks');
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(6,1,'Uncle Meat: Main Title Theme'),
(6,2,'The Voice of Cheese'),
(6,3,'Nine Types of Industrial Pollution'),
(6,4,'Zolar Czakl'),
(6,5,'Dog Breath, in the Year of the Plague'),
(6,6,'The Legend of the Golden Arches'),
(6,7,'Louie Louie'),
(6,8,'The Dog Breath Variations'),
(6,9,'Sleeping in a Jar'),
(6,10,'Our Bizarre Relationship'),
(6,11,'The Uncle Meat Variations'),
(6,12,'Electric Aunt Jemima'),
(6,13,'Prelude to King Kong'),
(6,14,'God Bless America'),
(6,15,'A Pound for a Brown on the Bus'),
(6,16,'Ian Underwood Whips It Out'),
(6,17,'Mr. Green Genes'),
(6,18,'We Can Shoot You'),
(6,19,'If We''d All Been Living in California...'),
(6,20,'The Air'),
(6,21,'Project X'),
(6,22,'Cruising for Burgers'),
(6,23,'King Kong');
INSERT INTO item_tracklist(album_id,track_id,track_name) VALUES
(7,1,'WPLJ'),
(7,2,'Igor''s Boogie, Phase One'),
(7,3,'Overture to a Holiday in Berlin'),
(7,4,'Theme from Burnt Weeny Sandwich'),
(7,5,'Igor''s Boogie, Phase Two'),
(7,6,'Holiday in Berlin, Full Blown'),
(7,7,'Aybe Sea'),
(7,8,'Little House I Used to Live in'),
(7,9,'Valarie'),
(8,1,'Didja Get Any Onya?'),
(8,2,'Directly from My Heart to You'),
(8,3,'Prelude to the Afternoon of a Sexually Aroused Gas Mask'),
(8,4,'Toads of the Short Forest'),
(8,5,'Get a Little'),
(8,6,'The Eric Dolphy Memorial Barbecue'),
(8,7,'Dwarf Nebula Processional March & Dwarf Nebula'),
(8,8,'Oh No'),
(8,9,'My Guitar Wants to Kill Your Mama'),
(8,10,'The Orange County Lumber Truck'),
(8,12,'Weasels Ripped My Flesh');
