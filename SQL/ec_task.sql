CREATE DATABASE ec_task DEFAULT CHARACTER SET utf8;

CREATE TABLE item(id INT PRIMARY KEY AUTO_INCREMENT,
name varchar(256) DEFAULT NULL, 
artist_name varchar(256) DEFAULT NULL,
detail text DEFAULT NULL,
price int(11) DEFAULT NULL,
file_name varchar(256),
youtube_url varchar(256)
);

INSERT INTO item(id,name,artist_name,detail,price,file_name,youtube_url) VALUES
(1,'Freak Out!','Frank Zappa','���v���^���� | 01:00 Released June 27,1966',2269,'freakout.jpeg','https://www.youtube.com/embed/yEbjInR6irI'),
(2,'Absolutely Free','Frank Zappa','���^���� | 44:00 Released May 26,1967',1631,'jpeg','youtube.com/'),
(3,'Lumpy Gravy','Frank Zappa','���^���� | 31:45 Released May 13,1968',1406,'jpeg','youtube.com/'),
(4,'We re Only in It for the Money','Frank Zappa','���^���� | 39:15 Released March 4,1968',1631,'jpeg','com./'),
(5,'Cruising with Ruben & the Jets','Frank Zappa','���^���� | 40:34 Released December 2,1968','728','jpeg','com/'),
(6,'Uncle Meat','Frank Zappa','���^���� | 120:44 Released April 21,1969','1829','jpeg','com/'),
(7,'Burnt Weeny Sandwich','Frank Zappa','���^���� | 41:27 Released February 9,1970','1631','jpeg','com/'),
(8,'Weasels Ripped My Flesh','Frank Zappa','���^���� | 43:03 Released August 10,1970','1596','jpeg','com/'),
(9,'Chungas Revenge','Frank Zappa','���^���� | 40:22 Released October 23,1970','1631','jpeg','com/'),
(10,'Fillmore East-June 1971','Frank Zappa','���^���� | 43:11 Released August 2,1971','1631','jpeg','com/'),
(11,'Just Another Band from L.A','Frank Zappa','���^���� | 45:18 Released March 26,1972','1430','jpeg','com/'),
(12,'Waka Jawaka','Frank Zappa','���^���� | 36:08 Released July 5,1972','1910','jpeg','com/'),
(13,'The Grand Wazoo','Frank Zappa','���^���� | 37:00 Released November 27,1972','1260','jpeg','com/'),
(14,'Over-Nite Sensation','Frank Zappa','���^���� | 34:37 Released September 7,1973','1631','jpeg','com/'),
(15,'Apostrophe','Frank Zappa','���^���� | 32:02 Released March 22,1974','1631','jpeg','com/'),
(16,'Roxy & Elsewhere','Frank Zappa','���^���� | 68:04 Released September 10,1974','1540','jpeg','com/'),
(17,'One Size Fits All','Frank Zappa','���^���� | 42:05 Released June 25,1975','1903','jpeg','com/'),
(18,'Bongo Fury','Frank Zappa','���^���� | 41:05 Released October 2,1975','1248','jpeg','com/'),
(19,'Zoot Allures','Frank Zappa','���^���� | 41:32 Released October 20,1976','1807','jpeg','com/'),
(20,'Zappa in New York','Frank Zappa','���^���� | 101:53 Released March 3,1978','1829','jpeg','com/'),
(21,'Studio Tan','Frank Zappa','���^���� | 39:18 Released September 15,1978','1298','jpeg','com/');
