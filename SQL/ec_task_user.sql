CREATE TABLE user (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  name varchar(256) DEFAULT NULL,
  login_id varchar(256) DEFAULT NULL,
  login_password varchar(256) DEFAULT NULL,
  create_date date DEFAULT NULL,
  update_date date DEFAULT NULL
);

INSERT INTO user (name,login_id,login_password,create_date,update_date)
VALUES (1,'admin','admin','password','2020-01-01','2020-01-01');
